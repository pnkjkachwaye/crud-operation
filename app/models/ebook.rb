class Ebook < ActiveRecord::Base
	validates :title, presence: true
	validates :publish_after, presence: true
	validates :published_at, presence: true
	has_attached_file :cover
	validates_attachment_presence :cover
	validates_attachment_content_type :cover, content_type: /\Aimage/
	has_attached_file :book
	validates_attachment_presence :book
	validates_attachment_content_type :book, content_type: "application/pdf"
end
