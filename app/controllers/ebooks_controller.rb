class EbooksController < ApplicationController

	def index
	    @ebooks = Ebook.all
	end

	def new
		@ebook = Ebook.new
	end


  	def create

  		@ebook = Ebook.new(ebook_params)

  		if @ebook.save
  			redirect_to @ebook
  		else
  			render 'new'
  		end		
 	end


 	 def edit
	    @ebook = Ebook.find(params[:id])
	 end

 	def show
    	@ebook = Ebook.find(params[:id])
 	 end

	def update
		 @ebook = Ebook.find(params[:id])
 
		  if @ebook.update(ebook_params)
		    redirect_to @ebook
		  else
		    render 'edit'
		  end
	end
	def destroy
   	
   	 @ebook = Ebook.find(params[:id])
    @ebook.destroy
 
    redirect_to @ebook
 	 end

 	private def ebook_params
 		params.require(:ebook).permit(:title,:published_at, :publish_after, :cover, :book)
 	end	
end
