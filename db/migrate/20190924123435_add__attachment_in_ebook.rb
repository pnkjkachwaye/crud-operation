class AddAttachmentInEbook < ActiveRecord::Migration
  def change
  	  add_attachment :ebooks, :cover  
  	  add_attachment :ebooks, :book  
  end
end
