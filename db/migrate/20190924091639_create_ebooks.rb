class CreateEbooks < ActiveRecord::Migration
  def change
    create_table :ebooks do |t|
      t.string :title
      t.boolean :published_at
      t.datetime :publish_after
      t.string :cover
      t.string :book
      t.timestamps null: false
    end
  end
end
