class ChangeFileStringToBeAttachmentInEbook < ActiveRecord::Migration
  def change
  	remove_column :ebooks, :cover, :string
  	remove_column :ebooks, :book, :string
  end
end
